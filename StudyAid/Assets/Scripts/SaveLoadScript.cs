﻿using UnityEngine;
using System.Collections;
using System.Runtime.Serialization.Formatters.Binary;
using System;
using System.IO;
using System.Collections.Generic;
using UnityEngine.UI;

public class SaveLoadScript : MonoBehaviour {

    public void Save()
        //This function gets the name of a deck from NameField, saves the current deck to C:\Users\"user"\AppData\LocalLow\StudyAid\StudyAid\
    {
        BinaryFormatter binary = new BinaryFormatter();
        //Creates a file with the text from the NameField inputfield
        InputField nameField = GameObject.Find("NameField").GetComponent<InputField>();
        FileStream fStream = File.Create(Application.persistentDataPath + "/" + nameField.text + ".deck");

        SaveManager saver = new SaveManager();
        saver.deck = FlashCardManager.deck;

        binary.Serialize(fStream, saver);
        fStream.Close();
        FlashCardManager.newDeck();
        nameField.text = "";    //Clears nameField text after save
    }
    public static void Load(string s)
    { //This function takes input from the grabfile filebrowser and loads it to be used in the gamescene.
        string address = grabfile.output;
        if(File.Exists(address))
        {
            BinaryFormatter binary = new BinaryFormatter();
            FileStream fStream = File.Open(address, FileMode.Open);
            SaveManager saver = (SaveManager)binary.Deserialize(fStream);
            fStream.Close();
            FlashCardManager.deck = saver.deck;
        }
    }
}
   [Serializable]
class SaveManager
{
    public List<FlashCard> deck;
}

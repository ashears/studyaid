﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using System.Linq;
using System;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;
using UnityEngine.EventSystems;

public class FlashCardManager : MonoBehaviour
{ //This class contains scripts to create and managage "flashcard" objects
    public static int i; // int value defaults to 0
    public static List<FlashCard> deck = new List<FlashCard>(); //This is the file which is saved and loaded as a deck of flashcards
    public int count = 0;
    public static System.Random rand = new System.Random();
    bool has_Image = false;//this might be a bad place to put this posssible error
    public static byte[] currentImagebytes;
    


    public static void newDeck() //Clears current deck
    {
        deck = new List<FlashCard>(); //This is the file which is saved and loaded as a deck of flashcards
    }

    public void newCard()
    //This function creates a FlashCard from input on New_Deck_Scene, then resets the input fields.
    //Implemented on Add to Deck button(NewCardButton)
    {
        InputField questionField = GameObject.Find("QuestionField").GetComponent<InputField>();
        InputField answerField = GameObject.Find("AnswerField").GetComponent<InputField>();
        InputField wrongField1 = GameObject.Find("WrongAnswer1Field").GetComponent<InputField>();
        InputField wrongField2 = GameObject.Find("WrongAnswer2Field").GetComponent<InputField>();
        InputField wrongField3 = GameObject.Find("WrongAnswer3Field").GetComponent<InputField>();
        Image imageField = GameObject.Find("ImageSelection").GetComponent<Image>();
        //Creates a FlashCard object and populates it with text from input Fields
        if (has_Image == false)
        {//Creates a non-image flashcard
            FlashCard newFlashCard = new FlashCard
            {
                hasImage = has_Image,
                Question = questionField.text,
                Answer = answerField.text,
                WrongAnswer1 = wrongField1.text,  
                WrongAnswer2 = wrongField2.text,
                WrongAnswer3 = wrongField3.text
            };
            deck.Add(newFlashCard);    //Adds flashcard to deck
        }
        if (has_Image == true)
        {//Creates an Image flashcard
            FlashCard newImageFlashCard = new FlashCard
            {
                hasImage = has_Image,
                cardImageBytes = currentImagebytes,
                Question = questionField.text,
                Answer = answerField.text,
                WrongAnswer1 = wrongField1.text,
                WrongAnswer2 = wrongField2.text,
                WrongAnswer3 = wrongField3.text
            };
            deck.Add(newImageFlashCard);    //Adds flashcard to deck
        }
        questionField.text = "";   //clears Input Fields
        answerField.text = "";     // "             "
        wrongField1.text = "";     // "             "
        wrongField2.text = "";     // "             "
        wrongField3.text = "";     // "             "
        imageField.sprite = null;  // "             "
    }

    public void grabImage()//Envokes the file browser
    {
        GameObject holder = GameObject.Find("grabscript_holder");
        holder.GetComponent<grabfile>().enabled = true;
    }

    public void enableimages()//Implemented on EnableImagesButton, New_Deck_Scene
    {
        GameObject.Find("ImageSelection").GetComponent<Image>().enabled = true;
        has_Image = true;
    }

    public void disableimages()//Implemented on DisableImagesButton, New_Deck_Scene
    {
        GameObject.Find("ImageSelection").GetComponent<Image>().enabled = false;
        has_Image = false;
    }

    public void grabNextCard()
    //The purpose of this function is to iterate through the list of flashcards. It is implemented on the EnterButton object.
    {
        FlashCard currentCard = deck[i];
        if (IncreaseCountSystem.selectedAnswer == currentCard.Answer) //Increases count if player got answer correct
        {
            count++;
            Text countText = GameObject.Find("count_text").GetComponent<Text>();
            countText.text = "Count = " + count;
        }
       
        Text correctAnswer = GameObject.Find("correctANS_text").GetComponent<Text>();
        correctAnswer.text = "Correct Answer was: " + currentCard.Answer;
 
        FlashCard previousCard = deck[i];
        while (deck[i] == previousCard) //Continues to select a random card 
        {
            init_Deck();
        }
    }

    public static void init_Deck() 
    {
        i = rand.Next(deck.Count); // random card from deck
        FlashCard currentCard = deck[i];
        if (currentCard.hasImage == true)
        {
            var texture2D = new Texture2D(2, 2);
            if (texture2D.LoadImage(currentCard.cardImageBytes))
            {
                GameObject imageselect = GameObject.Find("ImageSelection");
                Image _image = imageselect.GetComponent<Image>();
                _image.enabled = true;
                _image.sprite = Sprite.Create(texture2D,
                new Rect(0, 0, texture2D.width, texture2D.height),
                new Vector2(0.5f, 0.5f));
            }
        }else if(currentCard.hasImage == false)
        {
            GameObject.Find("ImageSelection").GetComponent<Image>().enabled = false;
        }
        Text questionText = GameObject.Find("QuestionText").GetComponent<Text>();
        questionText.text = currentCard.Question;

        Text Label1 = GameObject.Find("Label1").GetComponent<Text>();
        Text Label2 = GameObject.Find("Label2").GetComponent<Text>();
        Text Label3 = GameObject.Find("Label3").GetComponent<Text>();
        Text Label4 = GameObject.Find("Label4").GetComponent<Text>();

        List<string> answerChoices = new List<string>();    //Creates a list of all possible answers to be shuffled for each attempt
        answerChoices.Add(currentCard.Answer);              //
        answerChoices.Add(currentCard.WrongAnswer1);        //
        answerChoices.Add(currentCard.WrongAnswer2);        //
        answerChoices.Add(currentCard.WrongAnswer3);        //
        shuffle(answerChoices);                             //

        Label1.text = answerChoices.ElementAt(0);
        Label2.text = answerChoices.ElementAt(1);
        Label3.text = answerChoices.ElementAt(2);
        Label4.text = answerChoices.ElementAt(3);
    }

    public static void shuffle(IList<string> list)//This function is used to randomly organize a list of string
    {
        int n = list.Count;
        while (n > 1)
        {
            n--;
            int k = rand.Next(n + 1);
            string value = list[k];
            list[k] = list[n];
            list[n] = value;
        }
    }
}

[Serializable]
public class FlashCard
{
    public bool hasImage { get; set; }
    public byte[] cardImageBytes { get; set; }
    public string Question { get; set; }
    public string Answer { get; set; }
    public string WrongAnswer1 { get; set; }
    public string WrongAnswer2 { get; set; }
    public string WrongAnswer3 { get; set; }
}
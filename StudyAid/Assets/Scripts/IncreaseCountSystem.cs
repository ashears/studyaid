﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class IncreaseCountSystem : MonoBehaviour {
    //The purpose of this script is to check which of the multiple choice toggle options are selected
    public Toggle is_toggle1;
    public Toggle is_toggle2;
    public Toggle is_toggle3;
    public Toggle is_toggle4;
    public static string selectedAnswer;

    public void ActiveToggle()
    {
        if(is_toggle1.isOn)
        {
            selectedAnswer = GameObject.Find("Label1").GetComponent<Text>().text;
        }
        else if (is_toggle2.isOn)
        {
            selectedAnswer = GameObject.Find("Label2").GetComponent<Text>().text;
        }
        else if (is_toggle3.isOn)
        {
            selectedAnswer = GameObject.Find("Label3").GetComponent<Text>().text;
        }
        else if (is_toggle4.isOn){
            selectedAnswer = GameObject.Find("Label4").GetComponent<Text>().text;
        }
    }
}

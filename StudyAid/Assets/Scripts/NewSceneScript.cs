﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

//The purpose of this class is to hold functions which are used to change the current scene.
public class NewSceneScript : MonoBehaviour
{

    public void GoToScene(string scene)
    {
        SceneManager.LoadScene(scene);
    }

    public static void static_GoToScene(string scene) 
    {
        SceneManager.LoadScene(scene);
    }

    public void GoToNew_Deck_Scene()
    {
        FlashCardManager.newDeck();
        SceneManager.LoadScene("New_Deck_Scene"); 
       
    }
}

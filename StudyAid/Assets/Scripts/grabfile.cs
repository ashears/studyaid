﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using System.IO;

//Much of this code was gotten from the C# File Browser asset, available on the unity asset store: https://www.assetstore.unity3d.com/en/#!/content/18308

public class grabfile : MonoBehaviour
{
    public Texture2D file, folder, back, drive;
    public System.Random rand = new System.Random();
    string[] layoutTypes = { "Type 0", "Type 1" };
    
    FileBrowser fb = new FileBrowser();    //initialize file browser

    public static string output;
    // Use this for initialization
     void Start()
    {
        fb.setDirectory(Application.persistentDataPath);
        fb.fileTexture = file;
        fb.directoryTexture = folder;
        fb.backTexture = back;
        fb.driveTexture = drive;
        //show the search bar
        fb.showSearch = false;
        //search recursively (setting recursive search may cause a long delay)
        //fb.searchRecursively = true;
    }

    void OnGUI()
    {
        //All this block of code can be deleted, the only thing useful is deciding whether to use layout 1 or 2
        GUILayout.BeginHorizontal();
        GUILayout.BeginVertical();
        GUILayout.Label("Layout Type");
        fb.setLayout(GUILayout.SelectionGrid(fb.layoutType, layoutTypes, 1));
        GUILayout.Space(10);
        GUILayout.Space(10);
        // fb.showSearch = GUILayout.Toggle(fb.showSearch, "Show Search Bar");
        //fb.searchRecursively = GUILayout.Toggle(fb.searchRecursively, "Search Sub Folders");
        GUILayout.EndVertical();
        GUILayout.Space(10);
        GUILayout.EndHorizontal();

        //draw and display output
        if (fb.draw())
        { //true is returned when a file has been selected
          //the output file is a member if the FileInfo class, if cancel was selected the value is null
          //This part is my code
            if (fb.outputFile == null) {
                NewSceneScript.static_GoToScene("MenuScene");
            }
            if (fb.outputFile != null)
            {
                //fb.outputFile.ToString(); gives the whole data path while in the editor, but only gives the name of the file selected when ran as a standalone 
                // thus I added the data path to output when ran outside of the editor, which allows the program to execute correctly. 
                #if UNITY_EDITOR
                    output = fb.outputFile.ToString(); 
                #else
                    output = Application.persistentDataPath + "/" + fb.outputFile.ToString();
                #endif

                if (SceneManager.GetActiveScene().name == "GameScene") //When this script is used for GameScene, initialize fields in gamescene
                {
                    SaveLoadScript.Load(output);
                    FlashCardManager.init_Deck();
                }
                if (SceneManager.GetActiveScene().name == "New_Deck_Scene")
                {
                    
                    if (File.Exists(fb.outputFile.FullName))
                    {
                        var fileBytes = File.ReadAllBytes(fb.outputFile.FullName);
                        FlashCardManager.currentImagebytes = fileBytes;
                        var texture2D = new Texture2D(2, 2);
                        if (texture2D.LoadImage(fileBytes))
                        {
                            GameObject imageselect = GameObject.Find("ImageSelection");
                            Image _image = imageselect.GetComponent<Image>();
                            _image.sprite = Sprite.Create(texture2D,
                            new Rect(0, 0, texture2D.width, texture2D.height),
                            new Vector2(0.5f, 0.5f));
                        }
                    }
                }
                GameObject holder = GameObject.Find("grabscript_holder");
                holder.GetComponent<grabfile>().enabled = false;
            }
        }
    }
}